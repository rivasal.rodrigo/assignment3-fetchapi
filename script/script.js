/*@author Rodrigo Rivas */
'use strict';
document.addEventListener('DOMContentLoaded',setup);

let elements = [];

function setup(){
elements.h1 = document.querySelector('h1');
fetchData();
document.getElementById('get-quote').addEventListener('click',fetchData);
}

/**
 *Fetching information from the API
 * @param {String} url API's url to get the quotes
 * @param {object} response Holds the information either is positive or negative
 * @param {object} quote holds information if the reply is positive and handles it
 * @param {object} error catches the error en execute a function to deal with it
 */
function fetchData(){
    let url = 'https://ron-swanson-quotes.herokuapp.com/v2/quotes';
    fetch(url)
        .then(response =>{
            if(response.ok)
                return response.json();
            throw new Error("Status code: " + response.statusCode);
        })
        .then(quote => treatJSON(quote))
        .catch(error => treatError(error));
}

/**
 * This method treats the object gotten from the API
 * in this case it gets an array containing a String and after
 * it injects the string into the HTML.
 * @param {object} quote is an array containing a string fetched from the website
 * @param {element} elements.h1 element h1 gotten from the html
 */
function treatJSON(quote){
    elements.h1.textContent = "\""+quote[0]+ "\"";
}

/**
 * 
 * @param {object} error in case the repository returns an error
 * this function will be able to display what the error is in the 
 * console. 
 */
function treatError(error){
    elements.h1.textContent = error+", Please try again";
    console.log(error);
}

